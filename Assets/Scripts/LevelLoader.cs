﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private GameObject prefabsStand;
    [SerializeField] private float speedMovePlatform;
    private GameObject stand;
    private StandSetup platformSetup;
    private GameData gameData;

    public void Activate(){
        stand = ObjectsPool.Instance.GetObject(prefabsStand,Vector3.zero, Quaternion.identity);
        platformSetup = stand.GetComponent<StandSetup>();
        LoadFileData();
        LoadNextLevel();
        GameController.NextLevelAction += LoadNextLevel;
    }
    public void Restart(){
        platformSetup.DestroyCube();
        LoadNextLevel();
    }
    private void LoadNextLevel()
    {
        GameController.CountBall = GameController.CountLevel * 3;
        InstantiateCube(stand.transform);
        StartCoroutine(MovePlatform(stand));
    }
    private void InstantiateCube(Transform parrent)
    {
        if (gameData.levelsData.Count==GameController.CountLevel)
        {
            GameController.CountBall = 0;
            return;
        }
        var LevelData = gameData.levelsData[GameController.CountLevel - 1];
        GameController.AllCubeLevel = LevelData.localPositionCube.Count;
        platformSetup.Setup(LevelData.scaleWightPlatform, LevelData.scaleLengthPlatform, LevelData.localPositionCube);
    }

    private IEnumerator MovePlatform(GameObject obj)
    {
        while (obj.transform.position.y < 0)
        {
            obj.transform.Translate(Vector3.up * Time.deltaTime * speedMovePlatform);
            yield return null;
        }
        platformSetup.CubeActivate();
    }

    private void OnDestroy()
    {
        GameController.NextLevelAction -= LoadNextLevel;
    }

    private void LoadFileData()
    {
        var name = SettingGame.NameGameData;
        var asset = Resources.Load<TextAsset>(name);
        gameData = JsonUtility.FromJson<GameData>(asset.text);
    }

}
