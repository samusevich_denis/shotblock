﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static event Action ProgressAction;
    public static event Action NextLevelAction;
    public static event Action SetCountBallAction;
    public static event Action SetCountPointsAction;
    public static event Action GameOverAction;
    public static event Action PauseAction;
    private Coroutine gameOver;
    private static int countPoints;
    public static bool IsPauseActive
    {
        set
        {
            PauseAction?.Invoke();
        }
    }

    public static int CountPoints
    {
        get => countPoints;
        set
        {
            countPoints = value;
            SetCountPointsAction?.Invoke();
        }
    }

    private static int countBall;
    public static int CountBall
    {
        get => countBall;
        set
        {
            countBall = value;
            SetCountBallAction?.Invoke();
            if (countBall == 0)
            {
                GameOverAction?.Invoke();
            }
        }
    }
    public static int AllCubeLevel;
    private static int countCubeLevel;
    public static int CountCubeLevel
    {
        get => countCubeLevel;
        set
        {
            
            if (value > countCubeLevel)
            {
                countCubeLevel = value;
                return;
            }
            countCubeLevel = value;
            ProgressAction?.Invoke();
            if (countCubeLevel == 0)
            {
                CountLevel++;
                NextLevelAction?.Invoke();
            }
        }
    }

    public static int CountLevel { get; private set; }
    [SerializeField] private GameObject prefabProgressBar;
    [SerializeField] private GameObject prefabLevelLoader;
    [SerializeField] private GameObject prefabLevelObject;
    private ProgressBar progressBar;
    private LevelLoader levelLoader;
    private LevelObject levelObject;
    void Start()
    {
        CountBall = 0;
        CountPoints = 0;
        CountCubeLevel = 0;
        CountLevel = 1;
        GameOverAction += StartGameOver;
        var objProgressBar = Instantiate(prefabProgressBar);
        progressBar = objProgressBar.GetComponentInChildren<ProgressBar>();
        var objLevelLoader = Instantiate(prefabLevelLoader);
        levelLoader = objLevelLoader.GetComponentInChildren<LevelLoader>();
        var objLevelObject = Instantiate(prefabLevelObject, Vector3.zero, Quaternion.identity);
        levelObject = objLevelObject.GetComponentInChildren<LevelObject>();
        levelLoader.Activate();
        progressBar.Activate();
        levelObject.Activate();
    }

    private void Restart()
    {
        CountBall = 0;
        CountPoints = 0;
        countCubeLevel = 0;
        CountLevel = 1;
        progressBar.Restart();
        levelLoader.Restart();
    }
    private void StartGameOver()
    {
        if (gameOver != null)
        {
            StopCoroutine(gameOver);
        }
        gameOver = StartCoroutine(GameOver(3f));
    }

    private IEnumerator GameOver(float time)
    {
        while (time > 0)
        {
            yield return null;
            time -= Time.deltaTime;
            if (CountBall > 0)
            {
                StopCoroutine(gameOver);
            }
        }
        Restart();
    }

    private void OnDestroy()
    {
        GameOverAction -= StartGameOver;
    }

}
