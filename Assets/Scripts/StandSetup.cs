﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StandSetup : MonoBehaviour
{
    [SerializeField] private Transform platform;
    [SerializeField] private GameObject prefabCube;
    private Transform[] Cubes;
    public void Setup(float wight, float length, List<Vector3> localPositionCube)
    {
        var scale = platform.localScale;
        scale.x = wight;
        scale.z = length;
        platform.localScale = scale;
        float maxY = localPositionCube[0].y;
        Cubes = new Transform[localPositionCube.Count];
        for (int i = 0; i < localPositionCube.Count; i++)
        {
            var obj = ObjectsPool.Instance.GetObject(prefabCube);
            Cubes[i] = obj.transform;
            obj.transform.parent = transform;
            var rig = obj.GetComponent<Rigidbody>();
            rig.constraints = RigidbodyConstraints.FreezeAll;
            obj.transform.localPosition = localPositionCube[i];
            obj.transform.localScale = Vector3.one;
            obj.transform.rotation = Quaternion.identity;
            if (obj.transform.localPosition.y > maxY)
            {
                maxY = obj.transform.localPosition.y;
            }
        }
        transform.Translate(0f, -maxY - 0.5f, 0f);
    }

    public void CubeActivate()
    {
        for (int i = 0; i < Cubes.Length; i++)
        {
            var rig = Cubes[i].GetComponent<Rigidbody>();
            rig.constraints = RigidbodyConstraints.None;
            Cubes[i].parent = null;
        }
    }
    public void DestroyCube()
    {
        for (int i = 0; i < Cubes.Length; i++)
        {
            if (Cubes[i] != null)
            {
                Cubes[i].gameObject.SetActive(false);
            }
        }
    }
}

