﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public List<LevelData> levelsData = new List<LevelData>();
}

[System.Serializable]
public class LevelData
{
    public Vector3 positionStand;
    public float scaleWightPlatform;
    public float scaleLengthPlatform;
    public List<Vector3> localPositionCube = new List<Vector3>();

    public LevelData(GameObject stand)
    {
        positionStand = stand.transform.position;
        scaleWightPlatform = stand.transform.GetChild(0).localScale.x;
        scaleLengthPlatform = stand.transform.GetChild(0).localScale.z;
        for (int i = 2; i < stand.transform.childCount; i++)
        {
            localPositionCube.Add(stand.transform.GetChild(i).transform.localPosition);
        }
    }
}



