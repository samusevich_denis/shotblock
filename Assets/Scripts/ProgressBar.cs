﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{

    [SerializeField] private Image[] ImagesBar;
    [SerializeField] private Text nextLevelText;
    [SerializeField] private Text currentLevelText;
    [SerializeField] private Text CountBalls;
    [SerializeField] private Text CountPoints;

    private float stepProgress;
    private Color orange = new Color(1, 0.5f, 0f, 1f);
    private int indexProgresImage;

    public void Activate()
    {
        stepProgress = 1f / ImagesBar.Length;
        indexProgresImage = 0;
        ShowNextLevel();
        ShowCountBall();
        ShowCountPoints();
        GameController.NextLevelAction += ShowNextLevel;
        GameController.SetCountBallAction += ShowCountBall;
        GameController.SetCountPointsAction += ShowCountPoints;
        GameController.ProgressAction += ShowColorProgressLevel;
    }
    public void Restart()
    {
        indexProgresImage = 0;
        ShowNextLevel();
        ShowCountBall();
        ShowCountPoints();
        StartCoroutine(SetColorGray());
    }
    private void ShowColorProgressLevel()
    {
        var progres = (int)(((1 - (float)GameController.CountCubeLevel / GameController.AllCubeLevel) / stepProgress)+1f/GameController.AllCubeLevel);
        if (indexProgresImage < progres)
        {
            StartCoroutine(SetColorImage(progres));
        }
    }

    private void ShowNextLevel()
    {
        currentLevelText.text = $"{GameController.CountLevel}";
        nextLevelText.text = $"{GameController.CountLevel + 1}";
    }

    private IEnumerator SetColorImage(float progres)
    {
        for (int i = 0; i < ImagesBar.Length; i++)
        {
           if (progres>i && progres>indexProgresImage)
           {    
               yield return StartCoroutine(SetColorProgres(i));
               indexProgresImage = i;
           }  
           yield return null;
        }
        if (indexProgresImage == ImagesBar.Length - 1)
        {
            StartCoroutine(SetColorGray());
        }
    }
    private IEnumerator SetColorProgres(int indexImage)
    {
        while (1 - ImagesBar[indexImage].color.r > 0.05)
        {
            ImagesBar[indexImage].color = Color.Lerp(ImagesBar[indexImage].color, orange, Time.deltaTime*SettingGame.SpeedSetColorProgresBar);
            yield return null;
        }
        ImagesBar[indexImage].color = orange;
    }

    private IEnumerator SetColorGray()
    {
        indexProgresImage = 0;
        while (1 - ImagesBar[0].color.r < 0.45f)
        {
            for (int i = 0; i < ImagesBar.Length; i++)
            {
                ImagesBar[i].color = Color.Lerp(ImagesBar[i].color, Color.gray, Time.deltaTime*SettingGame.SpeedSetColorProgresBar);
                yield return null;
            }
        }
        for (int i = 0; i < ImagesBar.Length; i++)
        {
            ImagesBar[i].color = Color.gray;
            yield return null;
        }
    }
    private void ShowCountPoints()
    {
        CountPoints.text = GameController.CountPoints.ToString();
    }
    private void ShowCountBall()
    {
        CountBalls.text = GameController.CountBall.ToString();
    }
    private void OnDestroy()
    {
        GameController.NextLevelAction -= ShowNextLevel;
        GameController.SetCountBallAction -= ShowCountBall;
        GameController.SetCountPointsAction -= ShowCountPoints;
        GameController.ProgressAction += ShowColorProgressLevel;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Pause(bool isActive){
        GameController.IsPauseActive = isActive;
    }

}
