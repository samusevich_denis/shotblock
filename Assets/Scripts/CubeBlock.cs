﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBlock : DestroyElement
{
    private void OnEnable() {
        destroyObj = null;
        GameController.CountCubeLevel ++;
    }

    void Update()
    {
        if (destroyObj==null &&transform.parent==null&& transform.position.y <SettingGame.MinYForDestroy)
        {
            destroyObj = StartCoroutine(DestroyCube(0f));
        }
    }

    private  IEnumerator DestroyCube(float timeDelay)
    {
        GameController.CountPoints++;
        GameController.CountCubeLevel--;
        yield return StartCoroutine(DestroyGameElement(timeDelay));
    }
}
