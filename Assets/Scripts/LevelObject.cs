﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObject : MonoBehaviour
{
    [SerializeField] private GameObject wall;
    // Start is called before the first frame update
    public void Activate() {
        GameController.PauseAction +=SetActiveWall;
    }
    public void SetActiveWall(){
        wall.SetActive(!wall.activeSelf);
    }
    private void OnDestroy() {
        GameController.PauseAction -=SetActiveWall;
    }
}
