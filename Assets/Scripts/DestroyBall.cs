﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBall : DestroyElement
{

    private void OnEnable() {

        GameController.NextLevelAction+=NextLevel;
    }

    private void NextLevel(){
        destroyObj =  StartCoroutine(DestroyGameElement(0f));
    }

    void Update()
    {
        if (destroyObj==null&&transform.position.y < SettingGame.MinYForDestroy)
        {
            destroyObj= StartCoroutine(DestroyGameElement(5f));
        }
    }

    private void OnDisable() {
        GameController.NextLevelAction-=NextLevel;
    }
}
