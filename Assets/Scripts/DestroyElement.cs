﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyElement : MonoBehaviour
{
    protected Coroutine destroyObj;
    protected IEnumerator DestroyGameElement (float timeDelay)
    {
        while (transform.localScale.x>0.01f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,Vector3.zero,Time.deltaTime*SettingGame.SpeedScale);
            yield return null;
        }
        yield return new WaitForSeconds(timeDelay);
        transform.localScale = Vector3.one;
        destroyObj = null;
        gameObject.SetActive(false);
    }
}
