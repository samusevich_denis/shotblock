﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static event Action MouseButtonDownAction;
    void Update()
    {

        if (Input.GetMouseButtonDown(0)){
            MouseButtonDownAction?.Invoke();
        }  
    }
}
