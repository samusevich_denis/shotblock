﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;


public class LevelEditor : EditorWindow
{
    public GameData gameData;
    private Vector2 scrollPosition = Vector2.zero;

    [MenuItem("Shotblock/LevelEditor")]
    private static void ShowWindow()
    {
        var window = GetWindow<LevelEditor>();
        window.titleContent = new GUIContent("Level");
        window.Show();
    }
    private void Awake()
    {
        LoadFileData();
    }
    private void OnGUI()
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUIStyle.none);
        SerializedObject serializedObject = new SerializedObject(this);
        var serializedProperty = serializedObject.FindProperty("gameData");
        GUILayout.Label("GameData");
        var serializedArray = serializedProperty.FindPropertyRelative("levelsData");
        for (int i = 0; i < gameData.levelsData.Count; i++)
        {
            GUILayout.Space(10);
            GUILayout.Label($"LevelData №{i}");
            EditorGUILayout.PropertyField(serializedArray.GetArrayElementAtIndex(i), true);
        }
        GUILayout.BeginHorizontal();
        if (GUILayout.Button($"SaveSubLevel"))
        {
            SaveLevel();
        }
        if (GUILayout.Button($"ResetGameData"))
        {
            ResetGameData();
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(50);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button($"SaveData"))
        {
            SaveFileData();
        }
        if (GUILayout.Button($"LoadData"))
        {
            LoadFileData();
        }
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();
    }

    private void SaveLevel()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }
        var LevelData = new LevelData(obj);
        if (gameData.levelsData.Count == 0)
        {
            gameData.levelsData.Add(LevelData);
            return;
        }
        gameData.levelsData.Add(LevelData);
    }

    private void ResetGameData()
    {
        gameData = new GameData();
    }
    
    private void LoadFileData()
    {
        string filePath = $"{Application.dataPath}{SettingGame.GameDataPath}";
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
    }

    private void SaveFileData()
    {
        string dataAsJson = JsonUtility.ToJson(gameData);
        var filePath = $"{Application.dataPath}{SettingGame.GameDataPath}";
        File.WriteAllText(filePath, dataAsJson);
        AssetDatabase.Refresh();
    }
}
