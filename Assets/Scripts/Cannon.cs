﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField] private Transform pointShot;
    [SerializeField] private GameObject prefabBall;
    [SerializeField] private Transform HorizontalAxisX;

    private Coroutine moveCannon;

    private Camera cameraGame;

    void Start()
    {
        cameraGame = Camera.main;
        InputController.MouseButtonDownAction += ShotCannon;
    }

    private void ShotCannon()
    {
        if(moveCannon!=null){
            return;
        }
        var ray = cameraGame.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(ray.origin, ray.direction, out RaycastHit hit))
        {
            return;
        }
        if (hit.transform.gameObject.layer != SettingGame.LayerWallMouseRaycast)
        {
            return;
        }
        if (GameController.CountBall==0)
        {
            return;
        }
        GameController.CountBall--;
        moveCannon = StartCoroutine(BulletLaunch(hit.point));
    }

    private IEnumerator BulletLaunch(Vector3 point)
    {
        yield return StartCoroutine(MoveCannon(point));
        CreateAndLaunch();
        yield return null;
        moveCannon = null;
    }

    private IEnumerator MoveCannon(Vector3 point)
    {
        var vectorTargetY = point - transform.position;
        vectorTargetY.Normalize();
        var VectorCannonHorizontal = transform.forward;
        var rotateY = Vector2.Angle(new Vector2(VectorCannonHorizontal.x, VectorCannonHorizontal.z), new Vector2(vectorTargetY.x, vectorTargetY.z));
        rotateY *= VectorCannonHorizontal.x > vectorTargetY.x ? -1 : 1;
        var vectorTargetX = point - HorizontalAxisX.position;
        vectorTargetX.Normalize();
        var VectorCannonVertical = HorizontalAxisX.right;
        var rotateX = Vector2.Angle(new Vector2(VectorCannonVertical.y, VectorCannonVertical.z), new Vector2(vectorTargetX.y, vectorTargetX.z));
        rotateX *= VectorCannonVertical.y > vectorTargetX.y ? -1 : 1;

        var rotateStart = transform.rotation;
        transform.Rotate(0, rotateY, 0);
        var rotateEnd = transform.rotation;
        transform.rotation = rotateStart;
        
        var rotateStartAxis = HorizontalAxisX.localRotation;
        HorizontalAxisX.RotateAround(HorizontalAxisX.position,HorizontalAxisX.up,rotateX);
        var rotateEndAxis = HorizontalAxisX.localRotation;
        HorizontalAxisX.localRotation = rotateStartAxis; 

        float timeMove = 0;
        while(Mathf.Abs(transform.rotation.eulerAngles.y-rotateEnd.eulerAngles.y)>0.5f)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation,rotateEnd,SettingGame.SpeedMoveCannon*Time.deltaTime);
            HorizontalAxisX.localRotation = Quaternion.Lerp(HorizontalAxisX.localRotation,rotateEndAxis,SettingGame.SpeedMoveCannon*Time.deltaTime);
            timeMove += Time.deltaTime;
            yield return null;
        }
        transform.rotation = rotateEnd;
        HorizontalAxisX.localRotation= rotateEndAxis;

    }

    private void CreateAndLaunch()
    {
        var bullet = ObjectsPool.Instance.GetObject(prefabBall,pointShot.position, Quaternion.identity);
        bullet.transform.localScale = SettingGame.SizeBallCannon;
        var rigidbodyNextBullet = bullet.GetComponent<Rigidbody>();
        rigidbodyNextBullet.velocity = Vector3.zero;
        rigidbodyNextBullet.AddForce(pointShot.up *SettingGame.ForceShotCannon, ForceMode.Impulse);
    }
    
    private void OnDestroy()
    {
        InputController.MouseButtonDownAction -= ShotCannon;
    }
}
