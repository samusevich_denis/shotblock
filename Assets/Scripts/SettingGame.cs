﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SettingGame
{
    public static string NameGameData { get; set; } = "GameData";
    public static string GameDataPath { get; set; } = "/Resources/GameData.json";
    public static Vector3 SizeBallCannon { get; set; } = new Vector3(0.5f,0.5f,0.5f);
    public static int LayerWallMouseRaycast { get; set; } = 8;
    public static float ForceShotCannon  { get; set; } = 25;
    public static float TimeForMoveCannon { get; set; } = 1;
    public static float SpeedMoveCannon { get; set; } = 12;
    public static float MinYForDestroy  { get; set; } = 1;
    public static float SpeedScale { get; set; } = 1f;
    public static float SpeedSetColorProgresBar { get; set; } = 12f;
}
